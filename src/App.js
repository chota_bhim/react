import React, { Component } from 'react';
import './App.css'
class App extends Component {  
constructor(props) {
  super(props)
  this.state = {
    boxstyle : Array(9).fill(null),
    player : "X",
    winner_player : null,
    flag : false,
    draw : false
  }
}

  checkWinner()
  {
    let winner =
    [
      ["0","1","2"],
      ["3","4","5"],
      ["6","7","8"],
      ["0","3","6"],
      ["1","4","7"],
      ["2","5","8"],
      ["0","4","8"],
      ["2","4","6"]
    ]

    for (let i = 0; i<winner.length;i++)
    {
      const[a,b,c] = winner[i];
      if (this.state.boxstyle[a] && this.state.boxstyle[a] === this.state.boxstyle[b] && this.state.boxstyle[a] === this.state.boxstyle[c])
      {
       this.setState()
       {
        this.state.winner_player = this.state.player
        this.state.flag = true
       }

       alert(this.state.winner_player);
      }
    }
  }

  clear()
  {
    let b = this.state.boxstyle
    for (let i=0;i<this.state.boxstyle.length;i++)
    {
      b[i] = null
    }
    console.log(b)
    this.setState({
      boxstyle : b,
      winner_player : null,
      flag : false
    })
    console.log(this.state.boxstyle)
  }

  handleClick(index)
  {
    let b = this.state.boxstyle
    if (this.state.boxstyle[index] === null && this.state.winner_player===null)
    {
    b[index] = this.state.player
    let newPlayer = this.state.player === "X" ? "O" : "X"
    this.setState({
      boxstyle : b,
      player : newPlayer
    })
    for (let j= 0;j<this.state.boxstyle.length;j++)
    {
      if (this.state.boxstyle[j]===null)
      {
        this.state.draw = false
        break
      }
      else
        this.state.draw = true
    }
    }
    if (this.state.draw === true)
      {
        alert("Game is Draw !!!!!!")
        this.clear()
      }
    if (this.state.flag === false)
      this.checkWinner()
    else
      alert(this.state.winner_player)
  }
 render() {
const Box = this.state.boxstyle.map((box,index) => <div className="box" key={index} onClick={() => 
  this.handleClick(index)}> {box}</div>)
  return (
    <div className="main">  
     <h1>Welcome to Tic Tac Toe Game !!</h1>
     <button onClick={() => this.clear()}> Reset </button>
    <br></br>
    <br></br>
    <div className="boxstyle">
      {Box}
  </div>
  </div>

  );
}
}

export default App;
